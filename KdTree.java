//EDMalykh

public class KdTree {
	private Node root;
	private int size;
	
	private class Node {
		private Point2D point;
		private boolean xxOrient;
		private Node left, right;
		
		public Node(Point2D point, boolean xxOrient) { 
			this.point = point;
			this.xxOrient = xxOrient;
		}
	}
		
    // construct an empty set of points
	public KdTree() {
		size = 0;
	}
	
    // is the set empty?
	public boolean isEmpty() {
		return size == 0;
	}
	
    // number of points in the set
	public int size() {
		return size;
	}
    // add the point p to the set (if it is not already in the set)
	public void insert(Point2D p) {
		root = insert(root, p, true);
	}
	
	private Node insert(Node x, Point2D p, boolean xxOrient) {
        if (x == null) {  size++; return new Node(p, xxOrient); }
        if (x.point.equals(p)) return x;
        if (x.xxOrient) {
        	if (x.point.x() < p.x()) x.right = insert(x.right, p, !x.xxOrient);
        	if (x.point.x() >= p.x()) x.left = insert(x.left, p, !x.xxOrient);
        } else {
        	if (x.point.y() < p.y()) x.right = insert(x.right, p, !x.xxOrient);
        	if (x.point.y() >= p.y()) x.left = insert(x.left, p, !x.xxOrient);
        }
        return x;
	}
	
    // does the set contain the point p?
	public boolean contains(Point2D p) {
		return contains(root, p);
	}
	
	private boolean contains(Node x, Point2D p) {
        if (x == null) return false;
        if (x.point.equals(p)) return true;
        if (x.xxOrient) {
        	if (x.point.x() < p.x()) return contains(x.right, p);
        	if (x.point.x() >= p.x()) return contains(x.left, p);
        } else {
        	if (x.point.y() < p.y()) return contains(x.right, p);
        	if (x.point.y() >= p.y()) return contains(x.left, p);
        }
        return true;
	}
	
    // !!!draw all of the points to standard draw
	public void draw() {
		double[] minX = new double[size], minY = new double[size], maxX = new double[size], maxY = new double[size];
		Node[] drawArray  = new Node[size]; //array of points to draw		
		int lo = 0, hi = 0;
		Point2D p0, p1;
		drawArray[lo] = root;
		minX[lo] = 0; minY[lo] = 0; maxX[lo] = 1; maxY[lo] = 1;
		while (lo <= hi) {
	//		for (int i = lo; i <= hi; i++) 
	//			StdOut.print(size + "("+ drawArray[i].point.x() + "," + drawArray[i].point.y() + ") ");
			//add left kid
			if (drawArray[lo].left != null) {
				drawArray[++hi] = drawArray[lo].left; //add kid to drawArray
				//add kids rectangle
				if (drawArray[lo].xxOrient) { 
					maxX[hi] = drawArray[lo].point.x(); 
					minY[hi] = minY[lo]; minX[hi] = minX[lo]; maxY[hi] = maxY[lo]; 
				} else {
					maxY[hi] = drawArray[lo].point.y(); 
					minY[hi] = minY[lo]; minX[hi] = minX[lo]; maxX[hi] = maxX[lo];
				}
			}
			//add right kid
			if (drawArray[lo].right != null) {
				drawArray[++hi] = drawArray[lo].right; //add kid to drawArray
				//add kids rectangle
				if (drawArray[lo].xxOrient) { 
					minX[hi] = drawArray[lo].point.x(); 
					minY[hi] = minY[lo]; maxX[hi] = maxX[lo]; maxY[hi] = maxY[lo]; 
				} else {
					minY[hi] = drawArray[lo].point.y(); 
					maxY[hi] = maxY[lo]; minX[hi] = minX[lo]; maxX[hi] = maxX[lo];
				}
			}
			
			//choose line coordinates
			if (drawArray[lo].xxOrient) {
				StdDraw.setPenColor(StdDraw.RED);
				p0 = new Point2D(drawArray[lo].point.x(), minY[lo]);
				p1 = new Point2D(drawArray[lo].point.x(), maxY[lo]);
			} else {
				StdDraw.setPenColor(StdDraw.BLUE);
				p0 = new Point2D(minX[lo], drawArray[lo].point.y());
				p1 = new Point2D(maxX[lo], drawArray[lo].point.y());
			}
			//draw vertical or horizontal line
			drawArray[lo].point.drawTo(p1); 
			drawArray[lo].point.drawTo(p0);
	
			//draw point
			StdDraw.setPenColor(StdDraw.BLACK);
			StdDraw.setPenRadius(0.01);
			drawArray[lo].point.draw(); 
			StdDraw.setPenRadius();
			lo++;
		}
	}
	
    // all points in the set that are inside the rectangle
	public Iterable<Point2D> range(RectHV rect) {
		Stack<Point2D> stack = new Stack<Point2D>();
		return range(root, rect, stack);
	}
	
	private Stack<Point2D> range(Node x, RectHV rect, Stack<Point2D> stack) {
		if (x == null) return stack;
		if (rect.contains(x.point)) stack.push(x.point);
		if ((x.xxOrient) && (rect.xmin() <= x.point.x())) range(x.left, rect, stack);
		if ((x.xxOrient) && (rect.xmax() > x.point.x())) range(x.right, rect, stack);
		if ((!x.xxOrient) && (rect.ymin() <= x.point.y())) range(x.left, rect, stack);
		if ((!x.xxOrient) && (rect.ymax() > x.point.y())) range(x.right, rect, stack);
		return stack;
	}
	
    // a nearest neighbor in the set to p; null if set is empty
	public Point2D nearest(Point2D p) {
		return nearest(p, root, Double.MAX_VALUE, root.point);
	}

	private Point2D nearest(Point2D p, Node x, double min, Point2D nearP) {
		if (x == null) return nearP;
		if (p.distanceTo(x.point) < min) { nearP = x.point; min = p.distanceTo(nearP); } 
		if (x.xxOrient) {
			if (p.x() < x.point.x()) {
				nearP = nearest(p, x.left, min, nearP);
				min = p.distanceTo(nearP);
				if (min > Math.abs(p.x() - x.point.x())) return nearest(p, x.right, min, nearP);
			} else {
				nearP = nearest(p, x.right, min, nearP);
				min = p.distanceTo(nearP);
				if (min > Math.abs(p.x() - x.point.x())) return nearest(p, x.left, min, nearP);
			}
			
		} else {
			if (p.y() < x.point.y()) {
				nearP = nearest(p, x.left, min, nearP);
				min = p.distanceTo(nearP);
				if (min > Math.abs(p.y() - x.point.y())) return nearest(p, x.right, min, nearP);
			} else {
				nearP = nearest(p, x.right, min, nearP);
				min = p.distanceTo(nearP);
				if (min > Math.abs(p.y() - x.point.y())) return nearest(p, x.left, min, nearP);
			}
		}
		return nearP;
	}
/*	
	public static void main(String[] args) {
		KdTree tree  = new KdTree();
		String filename = args[0];
        In in = new In(filename);
        
        while (!in.isEmpty()) {
            double x = in.readDouble();
            double y = in.readDouble();
            Point2D p = new Point2D(x, y);
            tree.insert(p);
        }
        
        Point2D query = new Point2D(0.160547, 0.525781);
        tree.draw();
        StdDraw.setPenRadius(0.01);
        StdDraw.setPenColor(StdDraw.BLUE);
        query.draw();
        StdDraw.setPenColor(StdDraw.RED);
        tree.nearest(query).draw();
        StdOut.println("nearest to " + query + ": " + tree.nearest(query));
        StdOut.println("size: " + tree.size());
        StdOut.println("contains " + query + ": " + tree.contains(query));
	}
*/
}